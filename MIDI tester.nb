(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3448,         89]
NotebookOptionsPosition[      2604,         70]
NotebookOutlinePosition[      2945,         85]
CellTagsIndexPosition[      2902,         82]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"StringJoin", "[", " ", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"I", "'"}], "m", " ", "sorrry"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
     "#", "<>", "\"\< = Animator.StringToHash(\\\"\>\"", "<>", "#", "<>", 
      "\"\<\\\");\\n\>\""}], ")"}], "&"}], "/@", 
   RowBox[{"ToUpperCase", "[", 
    RowBox[{"Alphabet", "[", "]"}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7434782656191187`*^9, 3.7434784580627613`*^9}, {
  3.7434788276834145`*^9, 
  3.743478869022025*^9}},ExpressionUUID->"c136996e-3570-4993-8d59-\
65dcf0a78979"],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<RiffRaft`\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.741045871678807*^9, 3.74104587750229*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"ff6266c7-8439-43cd-910e-563e47e0d53a"],

Cell[BoxData[
 RowBox[{"LoadAIMSPP", "[", "]"}]], "Input",
 CellChangeTimes->{{3.741045892287592*^9, 3.7410458961087503`*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"7f1a922b-4821-43df-a56d-f2b84e2172df"],

Cell[BoxData[
 RowBox[{"LoadMIDIFile", "[", "\"\<Tester MIDI\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.7410462123729415`*^9, 3.7410462172870045`*^9}, {
  3.741093233935177*^9, 3.741093236918065*^9}, {3.7410947936843877`*^9, 
  3.7410947969262996`*^9}, {3.7422227839515533`*^9, 3.742222785148543*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"49e1a7bb-2c11-4a77-9a37-7c8c8b7e3c94"],

Cell[BoxData[
 RowBox[{"StartAIMSPP", "[", "]"}]], "Input",
 CellChangeTimes->{{3.741054137970805*^9, 3.741054141402523*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"7d340c02-5c05-4ef1-960e-e613c0c421ea"],

Cell[BoxData[
 RowBox[{"PlayMIDIFile", "[", "]"}]], "Input",
 CellChangeTimes->{{3.741052255593485*^9, 3.741052261932334*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"d921f135-d8f7-423b-ae5f-95cf4fd2a5d0"],

Cell[BoxData[
 RowBox[{"CloseAIMSPP", "[", "]"}]], "Input",
 CellChangeTimes->{{3.7422228370385447`*^9, 3.742222839021288*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"5881855d-188a-46d7-97b1-fc79fbe62f82"]
},
WindowSize->{551, 533},
WindowMargins->{{Automatic, 0}, {50, Automatic}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 609, 16, 67, "Input",ExpressionUUID->"c136996e-3570-4993-8d59-65dcf0a78979"],
Cell[1170, 38, 217, 3, 28, "Input",ExpressionUUID->"ff6266c7-8439-43cd-910e-563e47e0d53a"],
Cell[1390, 43, 204, 3, 28, "Input",ExpressionUUID->"7f1a922b-4821-43df-a56d-f2b84e2172df"],
Cell[1597, 48, 381, 5, 28, "Input",ExpressionUUID->"49e1a7bb-2c11-4a77-9a37-7c8c8b7e3c94"],
Cell[1981, 55, 203, 3, 28, "Input",ExpressionUUID->"7d340c02-5c05-4ef1-960e-e613c0c421ea"],
Cell[2187, 60, 204, 3, 28, "Input",ExpressionUUID->"d921f135-d8f7-423b-ae5f-95cf4fd2a5d0"],
Cell[2394, 65, 206, 3, 28, "Input",ExpressionUUID->"5881855d-188a-46d7-97b1-fc79fbe62f82"]
}
]
*)

