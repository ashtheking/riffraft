(* ::Package:: *)

(* Defines a new package - similar to a Class or Namespace in other languages. *)
(* Name "RiffRaft`", requires "UnityLink`" package. *)
BeginPackage["RiffRaft`", {"UnityLink`"}]


(*
Documentation References:
	UnityLink/ref/UnityTransform
   tutorial/SettingUpWolframLanguagePackages
;
Reminder: When exporting to package, convert to "Code" style.

Optional 'Quiet' parameters to overwrite global quiet flag.
LoadAIMSPP and StartAIMSPP both have 'quiet' parameters to set global flag.
StartAIMSPP has an optional 'lvl' parameter to automatically load the specified level.
*)


(* Method Documentation. Doubles as ' public' access headers. *)

(* Load/Close*)
LoadAIMSPP::usage = "Starts up Unity, etc.";
CloseAIMSPP::usage = "Closes Unity, resets directories, etc.";

(* Movement methods *)
StartAIMSPP::usage = "Tells AIMSPP to start listening for instructions.";
HardMoveAIMSPP::usage = "Forcibly moves AIMSPP by [{x, y, z}] units.";
ResetAIMSPP::usage = "Reset the position and rotation of AIMSPP, as well as the level.";
MoveAIMSPPAll::usage = "Move AIMSPP the specified [x,y,z,r,r] units. For use in scripts.";
MoveAIMSPPForward::usage = "Move AIMSPP forward [x] unit(s).";
MoveAIMSPPRightward::usage = "Move AIMSPP rightward [z] unit(s).";
MoveAIMSPPUpward::usage = "Move AIMSPP upward [y] unit(s).";
TurnAIMSPPSideways::usage = "Turn AIMSPP by [r] radians sideways (theta).";
TurnAIMSPPVertically::usage = "Turn AIMSPP by [r] radians vertically (phi).";
AIMSPPExecute::usage = "Sends all instructed commands to AIMSPP.";
AIMSPPPExecute::usage = "I'lll fix this sooon, maybe.";

(* Other methods *)
AIMSPPLoadLevel::usage = "Tells AIMSPP to load the specified level";
AIMSPPQuietly::usage = "Tell AIMSPP to [True/False] be quiet when invoking movement commands.";
AIMSPPSpeed::usage = "Print[] or Set[x] the movement speed of AIMSPP. Default value is 1.";
AIMSPPGetSpeed::usage = "Get[] the movement speed of AIMSPP. Default value is 1.";

(* Controller suppport *)
ControllerAIMSPP::usage = "Start[True] or Stop[False] the AIMSPP Dynamic Controller. Use with care!";

(* Switching individual panels *)
SwitchPanel::usage = "Tell AIMSPP to change the state of one of the panels on its back.";
ResetAllPanels::usage = "Tell AIMSPP to switch all panels on its back to their deactivated state.";

(* MIDI file stufff *)
LoadMIDIFile::usage = "Load up a MIDI file from the RiffRaft package directory, using a filename without the .mid extension.\nIf the optional second param is True, the filename is treated as a non-local file path (and neeeds to have the filename extension).";
PlayMIDIFile::usage = "Play the loaded MIDI file on AIMSPP's back.";
ShowMIDIFile::usage = "Display the currrently-loaded MIDI file.";

(*Animation stufff*)
RunAnimation::usage = "Animate AIMSPP by the first character of the input string";


(* Private 'Context' for unaccessible variables *)
Begin["`Private`"];


(* Private Variables *)
Private`started = False;
Private`inCinematic = True;
Private`moveList = {}; (* {forward, right, up, theta, phi} [unit / radian] *)
Private`unityLink = Null;
Private`globalQuiet = False;
Private`controllerActive = False;
Private`midiFile = Null;
Private`encodedMidiFile = {};


(* ::Text:: *)
(**)


(* Load/Close *)
LoadAIMSPP[quietly_:False] :=Module[{localDir},
	localDir = FileNameJoin[{DirectoryName[FindFile["RiffRaft`"]],"AIMSPP","RiffRaft"}];
	Private`unityProc = UnityOpen[localDir];

	Private`started = True;
	Print["AIMSPP Simulation, starting up!"];
	AIMSPPQuietly[quietly];
];
CloseAIMSPP[] := Module[{},
	If[Private`started , 
		CloseUnity[],
		Print["Warning, AIMSPP not actually started."]
	];
	Private`started = False;
	Print["AIMSPP Simulation, shutting down."];
];
StartAIMSPP[lvl_:"", quiet_:False, spd_:1] := Module[{tmp},
	If[Private`started,
		Print["AIMSPP starting up connection..."],(
			Print["Warning, AIMSPP not started!"];
			Return[]
		)
	];
	Private`unityLink = UnityListen[];
	Private`unityScene = FindUnityScene[];
	If[UnitySceneQ[Private`unityScene],(
			Print["AIMSPP connected to Unity!"];
			(* Find the Game Loader instance *)
			tmp = FindUnityGameObject["Game Loader"];
			tmp = UnityGameObjectComponents[tmp, "GameLoader"];
			
			(* Force a load of the 'start' scene *)
			If[PropertyValue[Private`unityScene, "Name"] == "main", (
				Print["Attempting to switch to 'start' scene..."];
				SetProperty[tmp, "SwitchScene" -> True];
				Private`unityScene = FindUnityScene[]
			)];
			(* Inform the Game Loader that we're connected *)
			SetProperty[tmp, "ConnectedToWolfram" -> True];
			
			(* Wait for the user to click a button *)
			Print["Awaiting choice of start mode!"];
			While[PropertyValue[tmp, "WolframWait"], Pause[1]];
			(* Wait for the scene to actually load *)
			While[PropertyValue[Private`unityScene, "Name"] != "main", Pause[1]];
			
			(* Now we can finally initalize variables *)
			Private`theCube = FindUnityGameObject["Cube"];
			Private`theScript = UnityGameObjectComponents[Private`theCube, "WolframInterface"];
			Private`colourScript = UnityGameObjectComponents[Private`theCube, "ColourChanger"];
			Private`animScript = UnityGameObjectComponents[Private`theCube, "AnimationScript"];
			
			(* Let the user know which mode they selected, and toggle functionality appropriately *)
			Private`inCinematic = PropertyValue[tmp, "Type"] == 1;
			Print["We are connected in ", If[Private`inCinematic, "Cinematic", "LINK"], " Mode!"]
		),
		Print["Something went wrong connecting, try again?"]
	];
	If[lvl != "", AIMSPPLoadLevel[lvl]];
	AIMSPPQuietly[quiet];
	If[spd != 1, AIMSPPSpeed[spd]];
];


(* Movement Methods *)
HardMoveAIMSPP[f_] := Module[{transform, position},
	If[Private`inCinematic, Return[]];
	transform = UnityGameObjectComponents[Private`theCube, "Transform"];
	position = PropertyValue[transform, "Position"];
	position += f;
	SetProperty[transform,  "Position" -> position];
	Print["Forcibly moving AIMSPP as instructed ( ", f, ")."];
];
ResetAIMSPP[] := Module[{},
	If[Private`inCinematic, Return[]];
	(* Command Code '1' - Reset Position & Rotation *)
	Private`moveList = Append[Private`moveList, {0, 0, 0, 0, 0, N[1]}];
	Print["Will reset AIMSPP and the level."];
];
MoveInstruction[x_:0, z_:0, y_:0, t_:0, p_:0, str_:"", quiet_:0] := Module[{isDeg, n, i, s, qu},
	If[Private`inCinematic, Return[]];
	isDeg = t != 0 || p != 0;
	If[!isDeg, (
		n = Which[x != 0, x, y != 0, y, z != 0, z];
		For[i = 0, i < n, i++,
			Private`moveList = Append[Private`moveList, {N[Sign[x]], N[Sign[z]], N[Sign[y]], 0, 0}]];
		s = ToString[n] <> " unit(s) "
	), (
		n = Which[t != 0, t, p != 0, p];
		Private`moveList = Append[Private`moveList, {0, 0, 0, N[t], N[p]}];
		s = ToString[n] <> " degrees "
	)];
	qu = If[quiet === 0, !Private`globalQuiet, !quiet]; (* Local, if set, overwrites Global *)
	If[qu, Print["Will move AIMSPP ", s, str, "."]];
];
MoveAIMSPPAll[x_:0, y_:0, z_:0, t_:0, p_:0, c_:0] := Module[{}, (
	Private`moveList = Append[Private`moveList, {N[x], N[z], N[y], N[t], N[p], c}];
	AIMSPPExecute[True]
)];
MoveAIMSPPForward[x_:1, quiet_:0] := Module[{}, MoveInstruction[x, 0, 0, 0, 0, "Forward", quiet]];
MoveAIMSPPRightward[z_:1, quiet_:0] := Module[{}, MoveInstruction[0, z, 0, 0, 0, "Rightward", quiet]];
MoveAIMSPPUpward[y_:1, quiet_:0] := Module[{}, MoveInstruction[0, 0, y, 0, 0, "Upward", quiet]];
TurnAIMSPPSideways[t_, quiet_:0] := Module[{}, MoveInstruction[0, 0, 0, t, 0, "Sideways", quiet]];
TurnAIMSPPVertically[p_, quiet_:0] := Module[{}, MoveInstruction[0, 0, 0, 0, p, "Vertically", quiet]];
AIMSPPExecute[quiet_:0] := Module[{qu},
	If[Private`inCinematic, Return[]];
	SetProperty[Private`theScript, "TheList" -> Private`moveList];
	SetProperty[Private`theScript, "HasChanges" -> True];
	Private`moveList = {};
	qu = If[quiet === 0, !Private`globalQuiet, !quiet]; (* Local, if set, overwrites Global *)
	If[qu, Print["Sent all instructions to AIMSPP. Watch!"]];
];
AIMSPPPExecute[quiet_:0] := Module[{qu},
	If[Private`inCinematic, Return[]];
	(
		SetProperty[Private`theScript,"hey"->(Round/@#1)];
		Pause[0.1];
	)&/@Private`moveList;
	Private`moveList = {};
	qu = If[quiet === 0, !Private`globalQuiet, !quiet]; (* Local, if set, overwrites Global *)
	If[qu, Print["Sent all instructions to AIMSPP. Watch!"]];
];


(* Other methods *)
AIMSPPLoadLevel[l_] := Module[{levelObj, levelScript},
	If[Private`inCinematic, Return[]];
	levelObj = FindUnityGameObject["LevelManager"];
	levelScript = UnityGameObjectComponents[levelObj, "LevelManager"];
	SetProperty[levelScript, "LevelName" -> l];
	SetProperty[levelScript, "HasChanges" -> True];
	Print["Told AIMSPP to load the \"", l, "\" level."];
];
AIMSPPQuietly[quiet_:True] := Module[{},
	If[Private`inCinematic, Return[]];
	Private`globalQuiet = quiet;
	If[quiet, Print["AIMSPP will be quiet in movement."]];
];
AIMSPPSpeed[val_:-1.47, quiet_:0] := Module[{qu},
	If[Private`inCinematic, Return[]];
	qu = If[quiet === 0, !Private`globalQuiet, !quiet]; (* Local, if set, overwrites Global *)
	If[val == -1.47, Print["AIMSPP movement speed is currently ", AIMSPPGetSpeed[], " units a second."],
		If[val < 0, Print["Speed must be >= 0!"],
			SetProperty[Private`theScript, "MoveSpeed" -> val];
			If[qu, Print["Set AIMSPP movement speed to ", val, " units a second."]]
		]
	];
];
AIMSPPGetSpeed[] := Module[{}, Return[PropertyValue[Private`theScript, "MoveSpeed"]]];


(* Controllers! *)
ControlHelper[state_] := Module[{x,y,p,t,z,command,spd, out = ""}, (
	If[!Private`controllerActive, Return[]];
	x=Floor[state[[1]]+0.5];
	y=Floor[state[[2]]+0.5];
	p = IntegerPart[IntegerPart[state[[3]]*10]/10.*15];
	t =IntegerPart[IntegerPart[state[[4]]*10]/10.*15];
	z = Abs[state[[5]]];
	command = If[state[[6]], 2, If[state[[7]], 1, 0]];
	If[z != 0 || command != 0, MoveAIMSPPAll[y,0,x,t,p, command]];
	spd = state[[8]];
	If[spd != 0, AIMSPPSpeed[Max[1, AIMSPPGetSpeed[] + spd], True]];

	If[x != 0, out= out <> "MoveAIMSPPRightward["<> ToString[x]<>"]\n"];
	If[y != 0, out= out <>"MoveAIMSPPForward["<> ToString[y] <> "]\n"];
	If[t != 0, out = out <> "TurnAIMSPPSideways[" <> ToString[t] <>"]\n"];
	If[p != 0, out = out <> "TurnAIMSPPVertically[" <> ToString[p] <>"]\n"];
	If[z != 0, out = out <> "AIMSPPExecute[]\n"];
	If[command != 0, out = out <> "Command " <> ToString[command] <> "\n"];
	If[spd != 0, out = out <> "AIMSPPSpeed[" <> ToString[spd] <>"]\n"];
	Text[Style[out, Italic, 12]]
)];
ControllerAIMSPP[op_] := Module[{},
	Private`controllerActive = If[op, True, False];
	If[Private`controllerActive, 
		Dynamic[ControlHelper[ControllerState[{"X1", "Y1", "Y2", "X2", "Z", "B7", "B8", "Y3"}]]]]
];


(* Panel switching, direct *)
SwitchPanel[name_String]:=
	SetProperty[Private`colourScript,"TogggleCommmands"->{name}];
	
SwitchPanel[name_String, on_:True]:=
	SetProperty[Private`colourScript,"TogggleCommmands"->{name<>If[on,"|1", "|0"]}];

SwitchPanel[commmands:{_String,_Symbol}..]:=
	SetProperty[Private`colourScript,"TogggleCommmands"->(
		#[[1]]<>If[#[[2]],"|1","|0"]&/@{commmands}
	)];

ResetAllPanels[] :=	
	SetProperty[Private`colourScript,"TogggleCommmands"->{"RESET PLZ|n thank u"}];


Private`CreateEncodedList[listOfSounds_] :=
Module[
	{output, eventOrdering, aNumber = 1000, noteNames, noteNumbers, singleEncode},
	
	(* First create a list of the (noteOn/noteOfff) events *)
	output = 
	Join[
		
		(* Start times *)
		List[
		
			(* This is the start time *)
			#[[2]][[1]],
			(* This is the note event *)
			{
				(* This is the note name, minus the octave number *)
				StringDrop[#[[1]], -1], 
				True (* Meaning it's a noteOn event *)
			}
		]&/@listOfSounds,
	
		(* End times *)
		List[
		
			(* This is the end time *)
			#[[2]][[2]],
			(* This is the note event *)
			{
				(* This is the note name, minus the octave number *)
				StringDrop[#[[1]], -1], 
				False (* Because it's a noteOfff event *)
			}
		]&/@listOfSounds
	];
	
	
	(* Then, we sort them. For that I make an eventOrdering function. *)
	
	eventOrdering[{time_,{note_,on_}}]:=
	(* This is the format that I save an event as. *)
		time + If[on, 0.001, 0];
		(* The 0.001 is to make sure alll noteOn events come
		after a simutaneous noteOfff event *)
	
	output = SortBy[output, eventOrdering];
	
	
	(* Next we neeed to encode each event into a number.
	First thing we neeed is an asssociation betweeen note names and
	corrresponding numbers. This is the ordering used in my enum. *)
	noteNames = {"A","A#","B","C","C#","D","D#","E","F","F#","G","G#"};
	noteNumbers = 
	Table[
		noteNames[[i]] -> i,
		{i,1,Length[noteNames]}
	];
	
	(* For convenience, I make a function to encode a single event *)
	singleEncode[{time_,{name_,on_}}]:=
		N[If[on,1,-1]*
		(
			(name/.noteNumbers)* aNumber
			+ time
		)];
	
	output = singleEncode /@ output;
	
	(* Appends a zero to the end to avoid SetProperty errors *)
	output = Append[output, 0];
	
	output
]


LoadMIDIFile[filename_String, notLocal_:False]:=
Module[
	{filepath, prevFile},
	filepath = 
	If[notLocal,
		filename,
	(*else*)
		FileNameJoin[{DirectoryName[FindFile["RiffRaft`"]],filename<>".mid"}]
	];
	
	prevFile = Private`midiFile;
	
	Private`midiFile = Import[filepath];
	If[Head[Private`midiFile] != Sound,
		Print["File load failed; either the file doesn't exist or it's not a MIDI file. Check your file path/name."];
		If[prevFile != Null,
			Private`midiFile = prevFile;
			Print["The previously-loaded file is still here."];
		],
	
	(* else *)
		Private`encodedMidiFile =
			Private`CreateEncodedList[Private`midiFile[[1]]];
		Private`encodedMidiFile
	]
];

PlayMIDIFile[]:=
Module[
	{},
	If[Head[Private`midiFile]==Sound,
		SetProperty[Private`colourScript,"EventList"-> Private`encodedMidiFile];
		EmitSound[Private`midiFile],
		
	(*else*)
		Print["File not found (or has beeen altered)"];
	];
];

ShowMIDIFile[]:=
If[Head[Private`midiFile]==Sound, Private`midiFile, Print["No sound file loaded."]];


(*The animation*)
RunAnimation[com_]:=
Module[
	{},
	SetProperty[Private`animScript,"commmand"-> com];
]


(* End implementation *)
End[ ];


(* Finishes defining the package. *)
EndPackage[]
