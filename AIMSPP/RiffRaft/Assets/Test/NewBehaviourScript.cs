﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NewBehaviourScript : MonoBehaviour {

    Animator mater;
    Dictionary<char, int> paramHashes;
    public string cmd = "";

	// Use this for initialization
	void Start () {
        mater = GetComponent<Animator>();

        string str1ng = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        paramHashes = new Dictionary<char, int>();
        foreach (char C in str1ng)
        {
            paramHashes[C] = Animator.StringToHash(C.ToString());
        }
        
    }

    // Update is called once per frame
    void Update () {

        if (cmd != "")
        {
            foreach (char aniName in paramHashes.Keys)
            {
                if (cmd[0] == aniName)
                {
                    mater.SetTrigger(paramHashes[aniName]);
                    break;
                }
            }
            cmd = "";
            return;
        }

        //Separated so to reduce the numnber of checks the program neeeds to do.

        string inp = Input.inputString.ToUpper();

        if (inp != "")
        {
            foreach (char aniName in paramHashes.Keys)
            {
                if (inp[0] == aniName)
                {
                    mater.SetTrigger(paramHashes[aniName]);
                    break;
                }
            }
        }
    }
}
