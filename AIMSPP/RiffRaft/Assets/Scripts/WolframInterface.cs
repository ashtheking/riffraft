﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class WolframInterface : MonoBehaviour
{
    // Move instruction list set by Mathematica
    public int[][] theList;
    // Boolean flipped to notify C# of changes
    public bool hasChanges;
    // The move speed of the character
    [Range(1.0f, 25.0f)]
    public float moveSpeed = 1.0f;

    public float waterHeight = 5.0f;

    /// <summary>
    /// Sorrry
    /// </summary>
    public int[] hey;

    public AIMSPP firstPerson;
    public GameObject thirdPerson;

    // Used for calling the functions once
    [SerializeField]
    private bool setupComplete;

    // Use this for initialization
    void Start()
    {
        theList = new int[1][];
        hasChanges = false;

        firstPerson = FindObjectOfType<AIMSPP>();
        firstPerson.enabled = false;
        thirdPerson.SetActive(false);
        setupComplete = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Get the game loader instance
        GameLoader loader = GameLoader.instance;
        // If we're not setup yet
        if (!setupComplete)
        {
            // Check if we are, invoke appropriate function
            if (loader.type == 0)
                OnConnection();
            else if (loader.type == 1)
                OnCinematic();
            return;
        }
        // If we're setup and in cinematic, do nothing
        if (setupComplete && loader.type == 1)
            return;

        if (hey != null && !firstPerson.startBlocker)
        {
            firstPerson.queue.Enqueue(hey);
            hey = null;
        }

        // If we've recieved new instructions
        // If we're moving to start, wait
        if (hasChanges && !firstPerson.startBlocker)
        {
            // Inform the console
            //Debug.Log("theList changed!");
            // Queue the new instructions
            for (int i = 0; i < theList.Length; i++)
                firstPerson.queue.Enqueue(theList[i]);
            // Reset the array
            hasChanges = false;
            theList = new int[1][];
        }
    }

    public void OnConnection()
    {
        Debug.Log("Loading LINK");
        // Enable AIMSPP
        firstPerson.enabled = true;
        // Reset position to the top, just in case
        firstPerson.transform.position = new Vector3(0, 6, 0);
        // Move AIMSPP to 0,0,0 in three seconds
        firstPerson.queue.Enqueue(new int[] { 0, 0, -2, 0, 0 });
        firstPerson.queue.Enqueue(new int[] { 0, 0, -2, 0, 0 });
        firstPerson.queue.Enqueue(new int[] { 0, 0, -2, 0, 0 });
        // Block other things from happening until that's done.
        firstPerson.startBlocker = true;
        // Also, Hide AIMSPP visually on startup
        firstPerson.SetAIMSPPVisibility(false);
        // Only ever call this function once
        setupComplete = true;
    }

    public void OnCinematic()
    {
        Debug.Log("Loading Cinematic!");
        // Enable the third person controller
        thirdPerson.SetActive(true);
        // Move "SepAnimated" to the third person object
        GameObject anim = GameObject.Find("SepAnimated");
        anim.transform.SetParent(thirdPerson.transform);
        anim.transform.localPosition = Vector3.zero;
        // Also, Hide AIMSPP visually on startup
        firstPerson.SetAIMSPPVisibility(false);
        // Change the camera to follow the third person
        SmoothFollow smooth = Camera.main.GetComponent<SmoothFollow>();
        smooth.target.SetParent(thirdPerson.transform);
        // Turn on any hidden meshes
        foreach (MeshRenderer render in smooth.target.GetComponentsInChildren<MeshRenderer>())
            render.enabled = true;
        smooth.height = 1.0f;
        // Only ever call this function once
        setupComplete = true;
    }

    /**
     * Print the contents of the Wolfram-sent Instructions.
     */
    string GetContents()
    {
        string s = "{ ";
        foreach (int[] arr in theList)
        {
            if (arr == null)
            {
                s += "Null, ";
                continue;
            }
            s += "{ ";
            for (int x = 0; x < arr.Length; x++)
            {
                s += arr[x];
                s += ", ";
            }
            s += "}, ";
        }
        s += "}";
        return s;
    }
}
