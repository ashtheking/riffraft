﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(WolframKernel))]
public class GameLoader : MonoBehaviour
{
    // Boolean flipped when Mathematica is connected
    public bool connectedToWolfram;

    // A boolean for Wolfram to listen to changes
    public bool wolframWait;

    // Boolean flipped when Mathematica needs to change scenes
    public bool switchScene;

    public int type = -1;

    // The Instance of this scene
    public static GameLoader instance;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            instance = this;

        //If instance already exists and it's not this, destroy it
        else if (instance != this)
            Destroy(this.gameObject);

        // Keep this instance around
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        // Await the connection boolean
        connectedToWolfram = false;
        // Tell wolfram to wait as well
        wolframWait = true;

        // Automatically open a connection on startup
        WolframKernel.Instance.Connect();
    }

    void Update()
    {
        // If Mathematica tells us to change scenes, switch scenes
        if (switchScene)
        {
            switchScene = false;
            SceneManager.LoadScene("start", LoadSceneMode.Single);
        }

        // Wolfram has made connection!
        if (connectedToWolfram)
        {
            // Reset the flag so it only occurs once
            connectedToWolfram = false;
            // Print the message!
            Debug.Log("Connected successfully!");
        }
    }

    public void OnLinkButton()
    {
        Debug.Log("Link");
        // Load the scene
        SceneManager.LoadScene("main", LoadSceneMode.Single);
        // We're on the LINK type
        type = 0;
        // Wolfram can stop waiting
        wolframWait = false;
    }

    public void OnCinematicButton()
    {
        Debug.Log("Cinematic");
        // Load the scene
        SceneManager.LoadScene("main", LoadSceneMode.Single);
        // We're in the cinematic type
        type = 1;
        // Wolfram can stop waiting
        wolframWait = false;
    }
}
