﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleWall : MonoBehaviour {

    public UIManager manager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AIMSPP>() != null || other.GetComponent<CinematicController>() != null)
            manager.CollideWarning();
    }
}
