﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CinematicController : MonoBehaviour {

    CharacterController controller;

    WolframInterface interOp;

    public bool DoRandom = false;


    Vector3 velocity, acceleration;
    public Vector3 steeringForce;
    public float mass = 3.47f;
    public float maxSpeed = 5f;
    public float maxForce = 5.5f;
    Vector3 wan; //Individual forces
    public float wanderAngle = 0f;
    public Vector3 wanderAngleVec;
    public float circDistance = 5.0f;
    public float circleRadius = 1.75f;
    public float angleChanger = 5.0f;

    public float wanWeight = 10.9f;
    public float cenWeight = 20.25f;

    // Use this for initialization
    void Start () {
        controller = GetComponent<CharacterController>();
        interOp = FindObjectOfType<WolframInterface>();


        velocity = new Vector3();
        acceleration = new Vector3();
        wan = new Vector3(0, 0);
        wanderAngleVec = fromAngle(wanderAngle);
    }
	
	// Update is called once per frame
	void Update () {
        // Have U turn the random on
        if(Input.GetKeyDown("u"))
            DoRandom = true;

        float forward = Input.GetAxis("Vertical");
        float rightward = Input.GetAxis("Horizontal");
        float upward = Input.GetAxis("Jump");
        float speedDiff = Input.GetAxis("Speed");

        if (Mathf.Abs(forward) > 0.1f || Mathf.Abs(rightward) > 0.1f || Mathf.Abs(upward) > 0.1f || Mathf.Abs(speedDiff) > 0.1f)
            DoRandom = false;

        if (DoRandom)
        {
            steeringForce *= 0;
            Wander();
            steeringForce += wan * wanWeight;
            //if (transform.position.x < -450 || transform.position.x > 450 || transform.position.z < -850 || transform.position.z > 550)
                steeringForce += Seek(Vector3.zero) * .2f * cenWeight;

            //limit this seeker's steering force to a maximum force
            steeringForce = limit(steeringForce, maxForce);

            //apply this steering force to the vehicle's acceleration
            ApplyForce(steeringForce);

            //add acceleration to velocity, limit the velocity, and add velocity to position
            velocity += (acceleration) * Time.deltaTime;
            velocity = limit(velocity, maxSpeed);
            velocity.y = 0.0f;
            transform.position += (velocity) * Time.deltaTime;

            //calculate forward and right vectors
            transform.forward = velocity.normalized;

            //reset acceleration
            acceleration *= 0;
        }
        else
        {
            interOp.moveSpeed += speedDiff;
            // Clamp speed
            interOp.moveSpeed = Mathf.Clamp(interOp.moveSpeed, 1.0f, 25.0f);

            // Rotate the transform by the left / right
            transform.Rotate(Vector3.up, rightward * 45.0f * Time.deltaTime);
            // Move up / down based on the upward - this is always slower
            controller.Move(transform.up * upward * interOp.moveSpeed * 0.1f * Time.deltaTime);
            // Move forward / back based on the forwards
            controller.Move(transform.forward * forward * interOp.moveSpeed * Time.deltaTime);
        }

        // Limit you to the height of the water
        float diff = (interOp.waterHeight - transform.position.y);
        if (diff < 0.05f)
            controller.Move(Vector3.up * -1.2f * Mathf.Abs(diff) * Time.deltaTime);
    }

    void ApplyForce(Vector3 force)
    {
        acceleration += force / mass;
    }

    Vector3 Seek(Vector3 target)
    {
        Vector3 seek = (target - transform.position).normalized * maxSpeed;
        return (seek - velocity).normalized;
    }

    void Wander()
    {
        wan = transform.forward;
        wan *= (circDistance);
        wanderAngle += Random.Range(-angleChanger, angleChanger);
        wan += (wanderAngleVec = fromAngle(wanderAngle) * circleRadius);
        wan = wan.normalized;
    }

    Vector3 fromAngle(float angle)
    {
        return new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0, Mathf.Sin(angle * Mathf.Deg2Rad));
    }

    Vector3 limit(Vector3 src, float max)
    {
        return src.magnitude > max ? src.normalized * max : src;
    }
}
