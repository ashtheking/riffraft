﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceCorrector : MonoBehaviour
{

    public void OnLink()
    {
        GameLoader loader = FindObjectOfType<GameLoader>();
        loader.OnLinkButton();
    }

    public void OnCinematic()
    {
        GameLoader loader = FindObjectOfType<GameLoader>();
        loader.OnCinematicButton();
    }
}
