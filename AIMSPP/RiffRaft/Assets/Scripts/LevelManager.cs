﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class LevelManager : MonoBehaviour
{

    public Material goalUnmet;
    public Material goalMet;
    public Material goalError;

    // Similar to AIMSPP, a set of variables
    // for Mathematica to update for level building
    public string levelName;
    public bool hasChanges;

    public GameObject goalPrefab;

    private List<Goalpost> goals;
    private int goalOrder;
    private bool outOfOrder;

    // Use this for initialization
    void Start()
    {
        if (goalUnmet == null || goalMet == null || goalError == null)
            throw new UnassignedReferenceException("Need materials set!");
        if (goalPrefab == null)
            throw new UnassignedReferenceException("Need goal prefab!");

        goals = new List<Goalpost>();
        goalOrder = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // If we get told to load a new level
        if (hasChanges)
        {
            // Load the level
            LoadLevel();
            // Clear the recieved flag
            hasChanges = false;
        }
    }

    public bool UpNext(int index)
    {
        // If we've gone out of order before, nothing is right anymore
        if (outOfOrder)
            return false;

        // Is this goalpost supposed to be hit next
        if (index == goalOrder)
        {
            // If so, we're good, update and continue
            goalOrder++;
            return true;
        }
        // Flip the boolean
        outOfOrder = true;
        // Uh-oh, we went out of order!
        return false;
    }

    public void ResetLevel()
    {
        goals.ForEach(e => e.ResetGoal());
        goalOrder = 0;
        outOfOrder = false;
    }

    private void LoadLevel()
    {
        // Clear the existing level, if any
        goals.ForEach(e => Destroy(e.gameObject));
        goals.Clear();
        goalOrder = 0;

        // List of goalposts to create, in order
        Queue<int[]> intQueue = new Queue<int[]>();
        int count = 0;

        // Build path if it's not explicit
        string path = Path.IsPathRooted(levelName) ? levelName : "Assets/Levels/" + levelName;
        // Load file
        StreamReader reader = new StreamReader(path);
        // If file is invalid, warn the user
        if (reader == null || reader.EndOfStream)
            throw new EndOfStreamException("Empty level file!");
        // Read file
        while (!reader.EndOfStream)
        {
            // While we have data left
            intQueue.Enqueue(
                reader.ReadLine() // Read file line
                .Split(',') // Split on comma
                .Select(e => int.Parse(e)) // Convert each to int
                .ToArray<int>() // Collect into array
                ); // Add to queue
        }
        reader.Close();

        // For all the queued goalposts
        while (intQueue.Count > 0)
        {
            // Get the next goalpost
            int[] data = intQueue.Dequeue();
            if (data.Length < 6)
            {
                // If it's invalid, continue
                Debug.Log("Invalid array: " + data);
                continue;
            }
            // Get the position
            Vector3 pos = new Vector3(data[0], data[1], data[2]);
            // And the rotation
            Quaternion rot = Quaternion.Euler(data[3], data[4], data[5]);
            // Make a new game object
            GameObject obj = Instantiate(goalPrefab, pos, rot);
            // Get that object's goalpost script
            Goalpost post = obj.GetComponent<Goalpost>();
            // Set the relevant index on the script
            post.SetIndex(count);
            // Add it to the global list of goalposts
            goals.Add(post);
            // Increment the count
            count++;
        }
    }
}
