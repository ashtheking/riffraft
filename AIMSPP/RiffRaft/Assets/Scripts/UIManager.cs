﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text positionLabel;
    public RectTransform context;
    public GameObject errorPanel;

    public RectTransform compass;

    public GameObject movementPrefab;
    public Sprite[] SpriteIndex;

    int count;
    AIMSPP linkInstance;
    CinematicController cineInstance;
    WolframInterface interOp;
    List<GameObject> moveList;

    float collideTimer = 0.0f;

    // Use this for initialization
    void Start()
    {
        count = 0;
        moveList = new List<GameObject>();
        errorPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        GameLoader loader = GameLoader.instance;
        if (loader.type == -1)
            return;
        else if (linkInstance == null || cineInstance == null || interOp == null)
        {
            interOp = FindObjectOfType<WolframInterface>();
            linkInstance = interOp.firstPerson;
            cineInstance = interOp.thirdPerson.GetComponent<CinematicController>();
        }

        GameObject instance = loader.type == 0 ? linkInstance.gameObject : cineInstance.gameObject;

        positionLabel.text = "";
        positionLabel.text += string.Format("{0:0.##}\n", instance.transform.position.x);
        positionLabel.text += string.Format("{0:0.##}\n", instance.transform.position.y);
        positionLabel.text += string.Format("{0:0.##}\n", instance.transform.position.z);
        positionLabel.text += string.Format("{0:0.#}\n", instance.transform.eulerAngles.x);
        positionLabel.text += string.Format("{0:0.#}\n", instance.transform.eulerAngles.y);
        positionLabel.text += string.Format("{0:0.#}\n", interOp.moveSpeed);

        if (collideTimer > 0.0f)
        {
            collideTimer -= Time.deltaTime;
            errorPanel.SetActive(true);
        }
        else
        {
            errorPanel.SetActive(false);
        }

        // Rotate the compass to match the direction of the player
        Vector3 originalRotation = compass.rotation.eulerAngles;
        originalRotation.z = instance.transform.rotation.eulerAngles.y;
        compass.rotation = Quaternion.Euler(originalRotation);
    }

    public void CollideWarning()
    {
        collideTimer = 5.0f;
    }

    public void ClearList()
    {
        foreach (GameObject o in moveList)
            Destroy(o);
        moveList.Clear();
        count = 0;
        context.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 300);
    }

    public void AddNewMovement(int[] obj)
    {
        GameObject i = Instantiate(movementPrefab, context);

        RectTransform transform = i.GetComponent<RectTransform>();
        Image image = i.transform.GetChild(0).gameObject.GetComponent<Image>();
        Text label = i.transform.GetChild(1).gameObject.GetComponent<Text>();

        int index = ArrToIndex(obj);

        transform.anchoredPosition = new Vector3(0, -30 - count * 50);
        label.text = IndexToName(index) + " " + IndexToValue(obj, index);
        image.sprite = SpriteIndex[index == 5 ? 0 : index == 6 ? 3 : index];
        transform.name = "Movement " + IndexToName(index);

        count++;
        if (count * 50 + 30 > context.rect.height)
            context.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, context.rect.height + 100);
        moveList.Add(i);
    }

    private string IndexToValue(int[] obj, int i)
    {
        GameLoader loader = GameLoader.instance;
        GameObject instance = loader.type == 0 ? linkInstance.gameObject : cineInstance.gameObject;
        float moveSpeed = interOp.moveSpeed;
        switch (i)
        {
            case 0: return (obj[i] * instance.transform.forward * moveSpeed).ToString();
            case 1: return (obj[i] * instance.transform.right * moveSpeed).ToString();
            case 2: return (obj[i] * instance.transform.up * moveSpeed).ToString();
            case 3: return obj[i] + " Degrees";
            case 4: return obj[i] + " Degrees";
            case 5: return (obj[0] * instance.transform.forward * moveSpeed + obj[1] * instance.transform.right * moveSpeed + obj[2] * instance.transform.up * moveSpeed).ToString();
            case 6: return obj[3] + " Deg, " + obj[4] + " Deg";
            default: return "???";
        }
    }

    private string IndexToName(int i)
    {
        switch (i)
        {
            case 0: return "Forward";
            case 1: return "Right";
            case 2: return "Upward";
            case 3: return "Sideways";
            case 4: return "Vertically";
            case 5: return "Movement";
            case 6: return "Rotation";
            default: return "???";
        }
    }

    private int ArrToIndex(int[] obj)
    {
        int index = -1;
        bool multMove = false;
        for (int i = 0; i < obj.Length; i++)
            if (obj[i] != 0)
            {
                if (index != -1)
                    multMove = true;
                index = i;
            }
        if (multMove)
            if (index < 3)
                return 5;
            else
                return 6;
        return index;
    }
}
