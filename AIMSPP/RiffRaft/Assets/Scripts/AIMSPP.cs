﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Utility;

[RequireComponent(typeof(CharacterController))]
public class AIMSPP : MonoBehaviour
{
    // Are we in the middle of the startup animation?
    public bool startBlocker;

    // The child transform the camera follows
    public Transform followTransform;

    // The movement queue
    public Queue<int[]> queue = new Queue<int[]>();
    // The character controller for movement
    private CharacterController controller;

    // The various game managers
    UIManager uiMngr;
    LevelManager lvlMngr;
    WolframInterface interOp;

    // The currently executing command
    [SerializeField]
    int[] current;
    // The percentage complete of that movement
    [SerializeField]
    float lerpPercentage;
    
    void Start()
    {
        controller = GetComponent<CharacterController>();

        uiMngr = FindObjectOfType<UIManager>();
        lvlMngr = FindObjectOfType<LevelManager>();
        interOp = FindObjectOfType<WolframInterface>();

        current = null;
        lerpPercentage = 0.0f;
    }
    
    void Update()
    {

        if (queue == null)
        {
            Debug.Log("How???");
            queue = new Queue<int[]>();
        }

        // If we've moved to 0,0,0 (within reason)
        if (startBlocker && (transform.position - Vector3.zero).magnitude < 0.01)
        {
            // Then stop blocking things
            startBlocker = false;
            Debug.Log("Finished!");
            // And show AIMSPP
            SetAIMSPPVisibility(true);
        }

        // Limit you to the height of the water
        if (!startBlocker)
        {
            float diff = (interOp.waterHeight - transform.position.y);
            if (diff < 0.05f)
                controller.Move(Vector3.up * -1.2f * Mathf.Abs(diff) * Time.deltaTime);
        }

        // If we've got instructions left
        if (queue.Count > 0 && current == null)
        {
            // Pop the instruction off the queue
            current = queue.Dequeue();

            // Is the instruction invalid?
            if (CheckIsEmpty() // Empty?
                || CheckCommandCode()) // Or if it's a reset
            {
                current = null; // Ensure current is empty
                return; // Return
            }

            // Set the lerp counter to 0
            lerpPercentage = 0.0f;
            // Update the queue
            if (!startBlocker) // If it's not the start movement
                uiMngr.AddNewMovement(current);
        }
        // If we've got an instruction we're lerping through
        if (current != null)
        {
            // Movement speed is ignored in the start animation
            float speed = startBlocker ? 1 : interOp.moveSpeed;

            // Evaluate the action and lerp
            EvaluateAction(Time.deltaTime, speed);

            // Update the lerp percentage
            lerpPercentage += Time.deltaTime;

            // If we've hit 100%, stop
            if (lerpPercentage + Time.deltaTime >= 1.0f)
            {
                // Move forwards the finishing remainder
                EvaluateAction(1.0f - lerpPercentage, speed);
                // Then clear the option
                current = null;
            }

            // If it's the initial movement, also move the sub transform
            if (startBlocker) // It starts at Y-1, and over three lerps reaches Y-0
                followTransform.position += (new Vector3(0, -1.0f / 3.0f, 0) * Time.deltaTime);
        }
    }

    /**
     * Check for an extra command unit
     */
    bool CheckCommandCode()
    {
        // Check if the array has a sixth element
        if (current.Length < 6)
            return false;

        // Command code 0 -- Not a command, ignore
        if(current[5] == 0)
            return false;

        // Command code 1 -- Reset world
        if (current[5] == 1)
        {
            // Reset position and rotation
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;

            // Also clear the UI instruction list
            uiMngr.ClearList();
            lvlMngr.ResetLevel();
        }

        // Command code 2 -- Reset rotations
        if (current[5] == 2)
        {
            transform.rotation = Quaternion.identity;
        }

        // Regardless, exit if it's a command instruction
        return true;
    }

    /**
     * Is the current instruction null or otherwise invalid
     */
    bool CheckIsEmpty()
    {
        // Is it null or too small?
        if (current == null || current.Length < 5)
            return true;
        // Now check the contents
        for (int i = 0; i < current.Length; i++)
            if (current[i] != 0) // Is there a non-zero value
                return false; // If so, we're good
        // It's entirely empty, and thus invalid
        return true;
    }

    /**
     * Evaluate the current action and move accordingly by a time delta and movement speed.
     */
    void EvaluateAction(float delta, float speed)
    {
        if (current[0] != 0)
        {
            controller.Move(current[0] * this.transform.forward * delta * speed);
        }
        if (current[1] != 0)
        {
            controller.Move(current[1] * this.transform.right * delta * speed);
        }
        if (current[2] != 0)
        {
            controller.Move(current[2] * this.transform.up * delta * speed);
        }
        if (current[3] != 0)
        {
            transform.Rotate(Vector3.up, current[3] * delta); // Don't change by speed
        }
        if (current[4] != 0)
        {
            transform.Rotate(Vector3.right, current[4] * delta); // Don't change by speed
        }
    }

    public void SetAIMSPPVisibility(bool visible)
    {
        foreach (MeshRenderer c in GetComponentsInChildren<MeshRenderer>())
            c.enabled = visible;
        foreach (LineRenderer c in GetComponentsInChildren<LineRenderer>())
            c.enabled = visible;
        foreach (SkinnedMeshRenderer c in GetComponentsInChildren<SkinnedMeshRenderer>())
            c.enabled = visible;
    }
}
