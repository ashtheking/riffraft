﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class DolphinInterfacer : MonoBehaviour
{

    public float[][] accelerations;
    public bool hasChanges = false;
    public float lengthMilliseconds = 1000f;

    // The character controller for movement
    private CharacterController controller;

    // Which index we're at
    [SerializeField]
    int index;
    // The percentage complete of that movement
    [SerializeField]
    float lerpPercentage;
    // Are we running
    [SerializeField]
    bool isRunning;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        index = -1;
        lerpPercentage = 0.0f;
        isRunning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasChanges)
        {
            index = 0;
            lerpPercentage = 0.0f;
            isRunning = true;
            hasChanges = false;
        }
        if (isRunning)
        {
            if (index >= accelerations.Length || index < 0)
            {
                isRunning = false;
                index = -1;
                lerpPercentage = 0.0f;
                return;
            }
            // Update the lerp percentage
            lerpPercentage += Time.deltaTime;


            // If we've hit 100%, stop
            if (lerpPercentage + Time.deltaTime >= lengthMilliseconds / 1000.0f)
            {
                index++;
                lerpPercentage = 0.0f;
                return;
            }
        }
    }

    private void EvaluateAction()
    {
        if (index >= accelerations.Length || index < 0)
            return;

    }
}
