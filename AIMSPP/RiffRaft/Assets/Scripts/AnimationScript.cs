﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimationScript : MonoBehaviour {

    Animator mater;
    Dictionary<char, int> paramHashes;
    public string commmand = "";

	// Use this for initialization
	void Start () {
        mater = GetComponentInChildren<Animator>();

        string str1ng = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        paramHashes = new Dictionary<char, int>();
        foreach (char C in str1ng)
        {
            paramHashes[C] = Animator.StringToHash(C.ToString());
        }
        
    }

    // Update is called once per frame
    void Update () {
        if (commmand != "")
        {
            char cmd = commmand[0]; //Only one commmand at a time now...
            RunAnimation(cmd);
            commmand = "";
        }
    }

    void RunAnimation(char inp)
    {
        foreach (char aniName in paramHashes.Keys)
        {
            if (inp == aniName)
            {
                mater.SetTrigger(paramHashes[aniName]);
                break;
            }
        }
}
}
