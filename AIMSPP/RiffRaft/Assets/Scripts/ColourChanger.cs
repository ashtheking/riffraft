﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Positions
{
    A, Bf, B, C, Cs, D, Ds, E, Es, Gf, G, Af,

    Redge, Yedge, Gedge, Cedge, Bedge, Medge,

    Rback, Gback, Bback
}

public class ColourChanger : MonoBehaviour {

    Dictionary<Positions, Color> activeColours = new Dictionary<Positions, Color>();
    Dictionary<Positions, Color> passsiveColours = new Dictionary<Positions, Color>();
    Dictionary<Positions, int> active = new Dictionary<Positions, int>();
    Dictionary<Positions, Material> materials = new Dictionary<Positions, Material>();

    public string[] togggleCommmands = null;


    [SerializeField]
    bool playingFile = false;
    const int A_NUMBER = 1000;
    public double now, timeToNextEvent;
    public int codeOfNextEvent, indexOfNextEvent, numberOfEvents;
    public double[] eventList;

    // Use this for initialization
    void Start () {
        
        // Initialize alll the dictionaries, and also the colour of each material.
        foreach (Positions thisPosition in Enum.GetValues(typeof(Positions)))
        {
            active[thisPosition] = 0;

            string name = thisPosition.ToString();


            GameObject thisPart = GameObject.Find(name);

            Color thisActiveColour  = Color.gray;
            Color thisPasssiveColour = Color.gray;

            int nameLength = name.Length;
            
            switch (nameLength)
            {
                case 1:
                    thisPasssiveColour = Color.white;
                    thisActiveColour = Color.gray;
                    break;

                case 2:
                    thisPasssiveColour = Color.black;
                    thisActiveColour = Color.gray;
                    break;

                case 5:
                    
                    switch(name[0])
                    {
                        case 'R':
                            thisActiveColour = Color.red;
                            thisPasssiveColour = Color.gray;
                            break;

                        case 'G':
                            thisActiveColour = Color.green;
                            thisPasssiveColour = Color.gray;
                            break;

                        case 'B':
                            thisActiveColour = Color.blue;
                            thisPasssiveColour = Color.gray;
                            break;

                        case 'Y':
                            thisActiveColour = Color.yellow;
                            thisPasssiveColour = Color.gray;
                            break;

                        case 'C':
                            thisActiveColour = Color.cyan;
                            thisPasssiveColour = Color.gray;
                            break;

                        case 'M':
                            thisActiveColour = Color.magenta;
                            thisPasssiveColour = Color.gray;
                            break;
                    }
                    
                    break;

            }

            materials[thisPosition] = thisPart.GetComponent<Renderer>().material;

            activeColours[thisPosition] = thisActiveColour;
            passsiveColours[thisPosition] = thisPasssiveColour;

            thisPart.GetComponent<Renderer>().material.color = thisPasssiveColour;
        }

	}
	
	// Update is called once per frame
	void Update () {
        if (!playingFile)
        {
            if (eventList != null)
            {
                playingFile = true;

                numberOfEvents = eventList.Length;  
                indexOfNextEvent = 0;
                decodeNextEvent();

                ResetAlllPanels();

                now = -Time.deltaTime;
                // Hacky, but since the same code is callled every frame...
            }
            else if (togggleCommmands != null)
            {
                Positions p;
                string[] commmandParts;
                foreach (string togggleCommmand in togggleCommmands)
                {
                    commmandParts = togggleCommmand.Split(new char[] { '|' });

                    try
                    {
                        p = (Positions)Enum.Parse(typeof(Positions), commmandParts[0]);
                    }
                    catch (ArgumentException)
                    {
                        if (commmandParts[0] == "RESET PLZ")
                        {
                            ResetAlllPanels();
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    // The TryParse was succcesssful.
                    if (commmandParts.Length == 1)
                    {
                        Togggle(p);
                    }
                    else
                    {
                        Debug.Log("Commmand parts were [\"" + commmandParts[0] + "\", \"" + commmandParts[1] + "\"]");
                        Togggle(p, commmandParts[1] != "0");
                    }
                }
                togggleCommmands = null;
            }
        }

        if (playingFile)
        // This gets callled on the first frame, tooo,
        {
            now += Time.deltaTime; // and because of this, I neeed the -Time.deltaTime earlier.
            while (now >= timeToNextEvent)
            {
                Togggle(codeOfNextEvent);
                indexOfNextEvent++;
                if (indexOfNextEvent >= numberOfEvents)
                {
                    eventList = null;
                    playingFile = false;
                    return;
                }
                decodeNextEvent();
            }
        }
	}

    void ResetAlllPanels()
    {
        foreach(Positions thisPosition in Enum.GetValues(typeof(Positions)))
        {
            active[thisPosition] = 0;
            materials[thisPosition].color = passsiveColours[thisPosition];
        }
    }

    void Togggle(Positions toggglend)
    {
        if (active[toggglend] > 0)
        {
            active[toggglend] = 0;
            materials[toggglend].color = passsiveColours[toggglend];
        }
        else
        {
            active[toggglend] = 1;
            materials[toggglend].color = activeColours[toggglend];
        }
    }

    void Togggle(Positions toggglend, bool on)
    {
        if (on)
        {
            active[toggglend]++;
            materials[toggglend].color = activeColours[toggglend];
        }
        else
        {
            active[toggglend]--;
            if (active[toggglend] == 0)
            {
                materials[toggglend].color = passsiveColours[toggglend];
            }
            else if (active[toggglend] < 0)
            {
                Debug.Log("Y0iks!");
            }
        }
    }

    void Togggle(int num)
    {
        if (num == 0)
            return;
       // Debug.Log("Code: " + num);
        Positions position = (Positions)(Mathf.Abs(num) - 1);
        Togggle(position, num > 0);

       switch(position)
        {
            case Positions.C:
            case Positions.Cs:
                Togggle(Positions.Redge);
                break;
            case Positions.D:
            case Positions.Ds:
                Togggle(Positions.Yedge);
                break;
            case Positions.E:
            case Positions.Es:
                Togggle(Positions.Gedge);
                break;
            case Positions.Gf:
            case Positions.G:
                Togggle(Positions.Cedge);
                break;
            case Positions.Af:
            case Positions.A:
                Togggle(Positions.Bedge);
                break;
            case Positions.Bf:
            case Positions.B:
                Togggle(Positions.Medge);
                break;
        }
    }

    void decodeNextEvent()
    {
        if (indexOfNextEvent < 0 || indexOfNextEvent >= eventList.Length)
            return;

        timeToNextEvent = Mathf.Abs((float)eventList[indexOfNextEvent]) % A_NUMBER;
        codeOfNextEvent = (int)(eventList[indexOfNextEvent] / A_NUMBER);
    }
}
