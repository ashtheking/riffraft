﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goalpost : MonoBehaviour
{

    public LevelManager manager;

    private int index;
    private bool passed;
    private MeshRenderer render;
    private Text text;

    // Use this for initialization
    void Start()
    {
        if (manager == null)
            manager = FindObjectOfType<LevelManager>();
        if (manager == null)
            throw new MissingReferenceException("Unable to find valid Level Manager in scene!");

        render = GetComponentInChildren<MeshRenderer>();
        render.material = manager.goalUnmet;
        text = GetComponentInChildren<Text>();
        text.text = "" + index;
    }

    void OnTriggerExit(Collider other)
    {
        // If it's not the turtle, don't bother
        if (other.gameObject.GetComponent<AIMSPP>() == null)
            return;
        // If we've been hit, ignore it
        if (passed)
            return;
        // Are we next in line
        if (manager.UpNext(index))
            // Good, mark the goal completed
            render.material = manager.goalMet;
        else
            // The player screwed up!
            render.material = manager.goalError;
        // We've been hit, reset
        passed = true;
    }

    // Only to be used by the Level Manager
    public void SetIndex(int i)
    {
        index = i;
    }

    // Used by the level manager to reset this goal
    public void ResetGoal()
    {
        // We're no longer done
        render.material = manager.goalUnmet;
        // We've not been passed before
        passed = false;
    }
}
