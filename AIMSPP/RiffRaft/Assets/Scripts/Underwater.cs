﻿using UnityEngine;
using System.Collections;


// Thanks to http://wiki.unity3d.com/index.php?title=Underwater_Script
public class Underwater : MonoBehaviour
{

    //This script enables underwater effects. Attach to main camera.

    //Define variable
    public float underwaterLevel = 5;

    //The scene's default fog settings
    private bool defaultFog;
    private Color defaultFogColor;
    private float defaultFogDensity;
    private Material defaultSkybox;
    private Material noSkybox;
    private Color defaultCameraColor;

    public Camera theCamera;
    public Color underwaterFog = new Color(0, 0.4f, 0.7f, 0.6f);
    public Color underwaterCamera = new Color(0, 0.4f, 0.7f, 1);
    public float underwaterFogDensity = 0.04f;

    void Start()
    {
        //Set the defaults
        defaultFog = RenderSettings.fog;
        defaultFogColor = RenderSettings.fogColor;
        defaultFogDensity = RenderSettings.fogDensity;
        defaultSkybox = RenderSettings.skybox;
        noSkybox = null;
        theCamera = GetComponent<Camera>();
        defaultCameraColor = theCamera.backgroundColor;
    }

    void Update()
    {
        if (transform.position.y < underwaterLevel)
        {
            RenderSettings.fog = true;
            RenderSettings.fogColor = underwaterFog;
            RenderSettings.fogDensity = underwaterFogDensity;
            RenderSettings.skybox = noSkybox;
            theCamera.backgroundColor = underwaterCamera;
        }
        else
        {
            RenderSettings.fog = defaultFog;
            RenderSettings.fogColor = defaultFogColor;
            RenderSettings.fogDensity = defaultFogDensity;
            RenderSettings.skybox = defaultSkybox;
            theCamera.backgroundColor = defaultCameraColor;
        }
    }
}